package com.example.demo.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Student1 {
	private int sno;
	private String sname;
	
	  @Autowired
	  @Qualifier("homeAddress")
	  private Address address;
	 
	
	public Student1() {
		super();
		System.out.println("Object created...");
	}
	public int getSno() {
		return sno;
	}
	public void setSno(int sno) {
		this.sno = sno;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	
	
	
	  public Address getAddress() { return address; } 
	  public void 	  setAddress(Address address) { this.address = address; }
	  
	  @Override public String toString() { return "Student [sno=" + sno +
	  ", sname=" + sname + ", address=" + address + "]"; }
	 
	 
	
	public void show() {
		System.out.println("In show method");
		address.toString();
	}
	

}
