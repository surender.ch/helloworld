package com.example.demo.model;

import org.springframework.stereotype.Component;

@Component("homeAddress")
public class HomeAddressImpl implements Address{
private String dno;
private String street;
private String city;
private String state;
public String getDno() {
	return dno;
}
public void setDno(String dno) {
	this.dno = dno;
}
public String getStreet() {
	return street;
}
public void setStreet(String street) {
	this.street = street;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
@Override
public String toString() {
	System.out.println("Inside home address toString()....");
	return "Address [dno=" + dno + ", street=" + street + ", city=" + city + ", state=" + state + "]";
}

}
