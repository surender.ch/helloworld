package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.example.demo.model.Student;


@SpringBootApplication
public class HelloworldApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(HelloworldApplication.class, args);
		Student s1 = context.getBean(Student.class);
		s1.show();
		Student s2 = context.getBean(Student.class);
		s2.show();
	}

}
